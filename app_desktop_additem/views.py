from django.shortcuts import render
from app_desktop_stock.models import Item, Toko
from .forms import AddItemForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from app_login.views import group_required

# Create your views here.
response = {}

@login_required
@group_required('Admin')
def index(request):
    form = AddItemForm()
    html = 'index_additem_desktop.html'
    return render(request, html, {'form' : form})

@login_required
@group_required('Admin')
def add_item(request):
    if request.method == "POST":
        form = AddItemForm(request.POST)
        if form.is_valid():
            nama = form.cleaned_data['nama']
            jumlah = form.cleaned_data['jumlah']
            hargaBeli = form.cleaned_data['hargaBeli']
            hargaJual = form.cleaned_data['hargaJual']
            tglBeli = form.cleaned_data['tglBeli']
            toko_id = form.cleaned_data['toko']
            toko = Toko.objects.get(id=toko_id) 

            #print(nama, jumlah, hargaBeli, hargaJual, tglBeli, tglJual, toko)
            item = Item(nama=nama, toko_dijual=toko, harga_beli=hargaBeli, harga_jual=hargaJual, jumlah_tersedia=jumlah, tanggal_beli=tglBeli, tanggal_jual=tglBeli, total_penjualan=0)
            item.save()
        else:
            print('tidak valid')
    else:
        form = AddItemForm()
        print('gagal')

    return HttpResponseRedirect('/add')
    
