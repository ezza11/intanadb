from django import forms
from app_desktop_stock.models import Item, Toko


class AddItemForm(forms.Form):
    def __init__(self, *args, **kwargs):
          super(AddItemForm, self).__init__(*args, **kwargs)
          self.fields['toko'] = forms.ChoiceField(choices=[ (o.id, str(o)) for o in Toko.objects.all()])
    
    nama = forms.CharField()
    jumlah = forms.IntegerField()
    hargaBeli = forms.IntegerField()
    hargaJual = forms.IntegerField()
    tglBeli = forms.DateField(widget=forms.TextInput(attrs=
                                {
                                    'id':'datepicker'
                                }))



