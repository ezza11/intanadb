from django.apps import AppConfig


class AppDesktopAdditemConfig(AppConfig):
    name = 'app_desktop_additem'
