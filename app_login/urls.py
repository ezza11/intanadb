from django.urls import path

from . import views

urlpatterns = [
    # URL -> localhost:8000/main
    path("", views.index, name='index'),
    path("login", views.auth, name='auth'),
    path("logout", views.logout_user, name='logout')
]
