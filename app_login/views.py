from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test

# Create your views here.
response = {}


def group_required(*group_names):
   """Requires user membership in at least one of the groups passed in."""

   def in_groups(u):
       if u.is_authenticated:
           if bool(u.groups.filter(name__in=group_names)) | u.is_superuser:
               return True
       return False
   return user_passes_test(in_groups)

def index(request):
    # TODO FIXING MOBILE LOGIN
    # if (request.user_agent.is_pc) :
    #     html = 'index_login_desktop.html'
    #     return render(request, html, response)
    # else :
    #     html = 'index_login_mobile.html'
    #     return render(request, html, response)
    html = 'index_login_desktop.html'
    return render(request, html, response)


def auth(request):
    if request.method == "POST" :
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            if is_admin(user):
                return HttpResponseRedirect('/dashboard')
            elif is_staff(user):
                return HttpResponseRedirect('/mobile/stock')
            else:
                return HttpResponseRedirect('/')
        # If form is empty
        else:
            return HttpResponseRedirect('/')
                
@login_required
def logout_user(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/')
    logout(request)
    return HttpResponseRedirect('/')


def is_admin(user):
    return user.groups.filter(name='Admin').exists()    

def is_staff(user):
    return user.groups.filter(name='Staff').exists()