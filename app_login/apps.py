from django.apps import AppConfig


class AppDesktopLoginConfig(AppConfig):
    name = 'app_desktop_login'
