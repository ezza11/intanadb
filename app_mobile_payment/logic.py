from app_desktop_dashboard.models import PendapatanHarian, PendapatanMingguan, PendapatanTahunan, KeuntunganHarian, KeuntunganMingguan,KeuntunganTahunan
import datetime
from app_desktop_stock.models import DailyItem

def hitung_total_profit(harga, jumlah, item):
    if harga == None:
        harga = item.harga_jual
        return jumlah * harga
    return jumlah * harga


def hitung_total_net(harga, jumlah, item):
    if harga == None:
        harga = item.harga_jual - item.harga_beli
        return jumlah * harga
    return jumlah * (harga - item.harga_beli)

def item_status_change(item, jumlah):
    item.total_penjualan += jumlah
    item.jumlah_tersedia -= jumlah
    item.save()

# Add profit to DB
def add_profit_daily(total, toko):
    pendapatan = PendapatanHarian.objects.get(id=1)
    if toko == "aPq":
        pendapatan.pendapatanToko1 += total
    else:
        pendapatan.pendapatanToko2 += total
    pendapatan.save()

def add_net_daily(total, toko):
    keuntungan = KeuntunganHarian.objects.get(id=1)
    if toko == "aPq":
        keuntungan.keuntunganToko1 += total
    else:
        keuntungan.keuntunganToko2 += total
    keuntungan.save()

def add_profit_weekly(total):
    now = datetime.datetime.now()
    pendapatan = PendapatanMingguan.objects.get(id=1)
    day = now.strftime("%A")
    if day == "Monday":
        pendapatan.pendapatanSenin += total
    elif day == "Tuesday":
        pendapatan.pendapatanSelasa += total
    elif day == "Wednesday":
        pendapatan.pendapatanRabu += total
    elif day == "Thursday":
        pendapatan.pendapatanKamis += total
    elif day == "Friday":
        pendapatan.pendapatanJumat += total
    elif day == "Saturday":
        pendapatan.pendapatanSabtu += total
    elif day == "Sunday":
        pendapatan.pendapatanMinggu += total
    else:
        print("error profit weekly")
    pendapatan.save()

def add_net_weekly(total):
    now = datetime.datetime.now()
    keuntungan = KeuntunganMingguan.objects.get(id=1)
    day = now.strftime("%A")
    if day == "Monday":
        keuntungan.keuntunganSenin += total
    elif day == "Tuesday":
        keuntungan.keuntunganSelasa += total
    elif day == "Wednesday":
        keuntungan.keuntunganRabu += total
    elif day == "Thursday":
        keuntungan.keuntunganKamis += total
    elif day == "Friday":
        keuntungan.keuntunganJumat += total
    elif day == "Saturday":
        keuntungan.keuntunganSabtu += total
    elif day == "Sunday":
        keuntungan.keuntunganMinggu += total
    else:
        print("error net weekly")
    keuntungan.save()

def add_profit_monthly(total):
    now = datetime.datetime.now()
    pendapatan = PendapatanTahunan.objects.get(id=1)
    month = now.strftime("%B")
    if month == "January":
        pendapatan.Jan += total
    elif month == "February":
        pendapatan.Feb += total
    elif month == "March":
        pendapatan.Mar += total
    elif month == "April":
        pendapatan.Apr += total
    elif month == "May":
        pendapatan.Mei += total
    elif month == "June":
        pendapatan.Jun += total
    elif month == "July":
        pendapatan.Jul += total
    elif month == "August":
        pendapatan.Aug += total
    elif month == "September":
        pendapatan.Sep += total
    elif month == "October":
        pendapatan.Okt += total
    elif month == "November":
        pendapatan.Nov += total
    elif month == "December" :
        pendapatan.Dec += total
    else :
        print('error profit montly')
    pendapatan.save()

def add_net_monthly(total):
    now = datetime.datetime.now()
    keuntungan = KeuntunganTahunan.objects.get(id=1)
    print(keuntungan)
    month = now.strftime("%B")
    if month == "January":
        keuntungan.Jan += total
    elif month == "February":
        keuntungan.Feb += total
    elif month == "March":
        keuntungan.Mar += total
    elif month == "April":
        keuntungan.Apr += total
    elif month == "May":
        keuntungan.Mei += total
    elif month == "June":
        keuntungan.Jun += total
    elif month == "July":
        keuntungan.Jul += total
    elif month == "August":
        keuntungan.Aug += total
    elif month == "September":
        keuntungan.Sep += total
    elif month == "October":
        keuntungan.Okt += total
    elif month == "November":
        keuntungan.Nov += total
    elif month == "December" :
        keuntungan.Dec += total
    else :
        print('error net montly')
    keuntungan.save()
    
def add_profit_all(total, id_toko):
    add_profit_daily(total, id_toko)
    add_profit_monthly(total)
    add_profit_weekly(total)

def add_net_all(total, id_toko):
    add_net_daily(total, id_toko)
    add_net_weekly(total)
    add_net_monthly(total)

def add_to_daily(item, jumlah):
    try:
        item = DailyItem.objects.get(item=item)
        item.jumlah += jumlah
        item.save()
    except DailyItem.DoesNotExist:
        item = DailyItem(item=item, jumlah=jumlah)
        item.save()
