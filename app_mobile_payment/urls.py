from django.urls import path

from . import views

urlpatterns = [
    # URL -> localhost:8000/main
    path("<str:id_toko>/", views.index, name='index'),
    path("<str:id_toko>/<int:id_item>", views.payment, name='payment'),
]
