from django import forms

class ConfirmationForm(forms.Form):
    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        super(ConfirmationForm, self).__init__(*args, **kwargs)
        # there's a `fields` property now
        self.fields['harga'].required = False
    jumlah = forms.IntegerField()
    harga = forms.IntegerField(required = False)