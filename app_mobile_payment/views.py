from django.shortcuts import render
from .forms import ConfirmationForm
from django.http import HttpResponseRedirect
from app_desktop_stock.models import Toko, Item, DailyItem
from django.urls import reverse
from .logic import hitung_total_net, hitung_total_profit, item_status_change, add_net_all, add_profit_all, add_to_daily
from django.contrib.auth.decorators import login_required
from app_login.views import group_required


# Create your views here.
response = {}

@login_required
@group_required('Staff', 'Admin')
def index(request, id_toko):
    form = ConfirmationForm()
    selected_toko = Toko.objects.get(unique_id=id_toko)
    items = selected_toko.items.all()
    response['form'] = form
    response['toko'] = selected_toko
    response['items'] = items
    html = 'index_payment_mobile.html'
    return render(request, html, response)

@login_required
@group_required('Staff', 'Admin')
def payment(request, id_toko, id_item):
    item = Item.objects.get(id=id_item)
    if request.method == "POST":
        form = ConfirmationForm(request.POST)
        if form.is_valid():
            jumlah = form.cleaned_data['jumlah']
            harga = form.cleaned_data['harga']
            if item.jumlah_tersedia < jumlah:
                # kasih error
                pass
            else:
                total_profit = hitung_total_profit(harga, jumlah, item)
                total_net = hitung_total_net(harga, jumlah, item)
                item_status_change(item, jumlah)
                add_net_all(total_net, id_toko)
                add_profit_all(total_profit, id_toko)
                add_to_daily(item, jumlah)

        else :
            print('tidak valid')
    else:
        form = ConfirmationForm()
    return HttpResponseRedirect(reverse('index', args=(id_toko,)))
    
