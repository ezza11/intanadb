from django.apps import AppConfig


class AppMobilePaymentConfig(AppConfig):
    name = 'app_mobile_payment'
