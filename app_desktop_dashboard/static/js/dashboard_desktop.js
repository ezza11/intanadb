function chartHarian1(datas) {
    var ctx = document.getElementById("chartHarian1").getContext('2d');
    var donutChart1 = new Chart(ctx, {
        type: 'pie',
        data : {
            datasets: [{
                data: datas,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                ],
                borderWidth: 1
            }],
        
            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Toko 1',
                'Toko 2',
            ]
        },
        options: {

        }
    });
}



function chartHarian2(datas) {
    var ctx = document.getElementById("chartHarian2").getContext('2d');
    var donutChart2 = new Chart(ctx, {
        type: 'pie',
        data : {
            datasets: [{
                data: datas,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                ],
                borderWidth: 1
            }],
        
            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Toko 1',
                'Toko 2',
            ]
        },
        options: {

        }
    });
}

function chartMingguan1(datas) {
    var ctx = document.getElementById("chartMingguan1").getContext('2d');
    new Chart(ctx,{
        type:"bar",
        data:{
            labels:["Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Mingu"],
            datasets:[{
                label:"Pendapatan",
                data:datas,
                fill:false,
                backgroundColor:[
                    "rgba(255, 99, 132, 0.2)",
                    "rgba(255, 159, 64, 0.2)",
                    "rgba(255, 205, 86, 0.2)",
                    "rgba(75, 192, 192, 0.2)",
                    "rgba(54, 162, 235, 0.2)",
                    "rgba(153, 102, 255, 0.2)",
                    "rgba(201, 203, 207, 0.2)"
                    ],
                borderColor:[
                    "rgb(255, 99, 132)",
                    "rgb(255, 159, 64)",
                    "rgb(255, 205, 86)",
                    "rgb(75, 192, 192)",
                    "rgb(54, 162, 235)",
                    "rgb(153, 102, 255)",
                    "rgb(201, 203, 207)"
                    ],
                borderWidth:1
            }]},
        options:{
            scales:{
                yAxes:[{"ticks":{"beginAtZero":true}}
            ]}}});
}

function chartMingguan2(datas) {
    var ctx = document.getElementById("chartMingguan2").getContext('2d');
    new Chart(ctx,{
        type:"bar",
        data:{
            labels:["Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Mingu"],
            datasets:[{
                label:"Keuntungan",
                data:datas,
                fill:false,
                backgroundColor:[
                    "rgba(255, 99, 132, 0.2)",
                    "rgba(255, 159, 64, 0.2)",
                    "rgba(255, 205, 86, 0.2)",
                    "rgba(75, 192, 192, 0.2)",
                    "rgba(54, 162, 235, 0.2)",
                    "rgba(153, 102, 255, 0.2)",
                    "rgba(201, 203, 207, 0.2)"
                    ],
                borderColor:[
                    "rgb(255, 99, 132)",
                    "rgb(255, 159, 64)",
                    "rgb(255, 205, 86)",
                    "rgb(75, 192, 192)",
                    "rgb(54, 162, 235)",
                    "rgb(153, 102, 255)",
                    "rgb(201, 203, 207)"
                    ],
                borderWidth:1
            }]},
        options:{
            scales:{
                yAxes:[{"ticks":{"beginAtZero":true}}
            ]}}});
}



// TODO chart tahunan masih belum 100 % bener
function chartTahunan1(datas) {
    var ctx = document.getElementById("chartTahunan1").getContext('2d');
    new Chart(ctx,{
        type:"line",
        data:{
            labels:["Jan","Feb","Mar","Apr","Mei","Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets:[{
                label:"Pendapatan",
                data:datas,
                fill:false,
                backgroundColor:[
                    "rgba(255, 99, 132, 0.2)",
                    "rgba(255, 159, 64, 0.2)",
                    "rgba(255, 205, 86, 0.2)",
                    "rgba(75, 192, 192, 0.2)",
                    "rgba(54, 162, 235, 0.2)",
                    "rgba(153, 102, 255, 0.2)",
                    "rgba(201, 203, 207, 0.2)",
                    "rgba(255, 205, 86, 0.2)",
                    "rgba(75, 192, 192, 0.2)",
                    "rgba(54, 162, 235, 0.2)",
                    "rgba(153, 102, 255, 0.2)",
                    "rgba(201, 203, 207, 0.2)"
                    ],
                borderColor:[
                    "rgb(255, 99, 132)",
                    "rgb(255, 159, 64)",
                    "rgb(255, 205, 86)",
                    "rgb(75, 192, 192)",
                    "rgb(54, 162, 235)",
                    "rgb(153, 102, 255)",
                    "rgb(201, 203, 207)",
                    "rgb(255, 205, 86)",
                    "rgb(75, 192, 192)",
                    "rgb(54, 162, 235)",
                    "rgb(153, 102, 255)",
                    "rgb(201, 203, 207)",
                    ],
                borderWidth:1
            }]},
        options:{
            scales:{
                yAxes:[{"ticks":{"beginAtZero":true}}
            ]}}});
}

function chartTahunan2(datas) {
    var ctx = document.getElementById("chartTahunan2").getContext('2d');
    new Chart(ctx,{
        type:"line",
        data:{
            labels:["Jan","Feb","Mar","Apr","Mei","Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets:[{
                label:"Keuntungan",
                data:datas,
                fill:false,
                backgroundColor:[
                    "rgba(255, 99, 132, 0.2)",
                    "rgba(255, 159, 64, 0.2)",
                    "rgba(255, 205, 86, 0.2)",
                    "rgba(75, 192, 192, 0.2)",
                    "rgba(54, 162, 235, 0.2)",
                    "rgba(153, 102, 255, 0.2)",
                    "rgba(201, 203, 207, 0.2)",
                    "rgba(255, 205, 86, 0.2)",
                    "rgba(75, 192, 192, 0.2)",
                    "rgba(54, 162, 235, 0.2)",
                    "rgba(153, 102, 255, 0.2)",
                    "rgba(201, 203, 207, 0.2)"
                    ],
                borderColor:[
                    "rgb(255, 99, 132)",
                    "rgb(255, 159, 64)",
                    "rgb(255, 205, 86)",
                    "rgb(75, 192, 192)",
                    "rgb(54, 162, 235)",
                    "rgb(153, 102, 255)",
                    "rgb(201, 203, 207)",
                    "rgb(255, 205, 86)",
                    "rgb(75, 192, 192)",
                    "rgb(54, 162, 235)",
                    "rgb(153, 102, 255)",
                    "rgb(201, 203, 207)",
                    ],
                borderWidth:1
            }]},
        options:{
            scales:{
                yAxes:[{"ticks":{"beginAtZero":true}}
            ]}}});
}