from django.apps import AppConfig


class AppDesktopDashboardConfig(AppConfig):
    name = 'app_desktop_dashboard'
