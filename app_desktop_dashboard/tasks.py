from celery import task, shared_task
from .models import KeuntunganHarian, KeuntunganMingguan, KeuntunganTahunan, PendapatanHarian, PendapatanMingguan, PendapatanTahunan

#PROFIT = Pendapatan

@shared_task
def reset_daily_profit():
    pendapatan = PendapatanHarian.objects.get(id=1)
    pendapatan.pendapatanToko1 = 0
    pendapatan.pendapatanToko2 = 0
    pendapatan.save()

@shared_task
def reset_daily_net():
    k = KeuntunganHarian.objects.get(id=1)
    k.keuntunganToko1 = 0
    k.keuntunganToko2 = 0
    k.save()

@shared_task
def reset_weekly_profit():
    p = PendapatanMingguan.objects.get(id=1)
    p.pendapatanSenin = 0
    p.pendapatanSelasa = 0
    p.pendapatanRabu = 0
    p.pendapatanKamis = 0
    p.pendapatanJumat = 0
    p.pendapatanSabtu = 0
    p.pendapatanMinggu = 0
    p.save()

@shared_task
def reset_weekly_net():
    k = KeuntunganMingguan.objects.get(id=1)
    k.pendapatanSenin = 0
    k.pendapatanSelasa = 0
    k.pendapatanRabu = 0
    k.pendapatanKamis = 0
    k.pendapatanJumat = 0
    k.pendapatanSabtu = 0
    k.pendapatanMinggu = 0
    k.save()

@shared_task
def reset_yearly_profit():
    p = PendapatanTahunan.objects.get(id=1)
    p.Jan = 0
    p.Feb = 0
    p.Mar = 0
    p.Apr = 0
    p.Mei = 0
    p.Jun = 0
    p.Jul = 0
    p.Aug = 0
    p.Sep = 0
    p.Okt = 0
    p.Nov = 0
    p.Dec = 0
    p.save()

@shared_task
def reset_yearly_net():
    k = KeuntunganTahunan.objects.get(id=1)
    k.Jan = 0
    k.Feb = 0
    k.Mar = 0
    k.Apr = 0
    k.Mei = 0
    k.Jun = 0
    k.Jul = 0
    k.Aug = 0
    k.Sep = 0
    k.Okt = 0
    k.Nov = 0
    k.Dec = 0
    k.save()