from django.contrib import admin
from .models import PendapatanHarian, PendapatanMingguan, PendapatanTahunan, KeuntunganHarian, KeuntunganMingguan, KeuntunganTahunan

# Register your models here.
admin.site.register(PendapatanHarian)
admin.site.register(PendapatanMingguan)
admin.site.register(PendapatanTahunan)
admin.site.register(KeuntunganHarian)
admin.site.register(KeuntunganMingguan)
admin.site.register(KeuntunganTahunan)