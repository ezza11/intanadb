from django.shortcuts import render
from .models import KeuntunganHarian, KeuntunganMingguan, KeuntunganTahunan, PendapatanHarian, PendapatanMingguan, PendapatanTahunan
from app_desktop_stock.models import Toko, Item, DiscrepancyItem, DailyItem
from django.contrib.auth.decorators import login_required
from app_login.views import group_required

# Create your views here.
response = {}

@login_required
@group_required('Admin')
def index(request):
    response['keuntunganHarian'] = KeuntunganHarian.objects.get(id=1)
    response['keuntunganMingguan'] = KeuntunganMingguan.objects.get(id=1)
    response['keuntunganTahunan'] = KeuntunganTahunan.objects.get(id=1)
    response['pendapatanHarian'] = PendapatanHarian.objects.get(id=1)
    response['pendapatanMingguan'] = PendapatanMingguan.objects.get(id=1)
    response['pendapatanTahunan'] = PendapatanTahunan.objects.get(id=1)
    response['depItem'] = DiscrepancyItem.objects.all()
    response['tokos'] = Toko.objects.all()
    response['best_sellers'] = Item.objects.order_by('-total_penjualan')[:10]
    response['daily'] = DailyItem.objects.all()
    html = 'index_dashboard_desktop.html'
    return render(request, html, response)