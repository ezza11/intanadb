from django.db import models

class PendapatanHarian(models.Model):
    pendapatanToko1 = models.IntegerField(default=0)
    pendapatanToko2 = models.IntegerField(default=0)

class KeuntunganHarian(models.Model):
    keuntunganToko1 = models.IntegerField(default=0)
    keuntunganToko2 = models.IntegerField(default=0)

class PendapatanMingguan(models.Model):
    pendapatanSenin = models.IntegerField(default=0)
    pendapatanSelasa = models.IntegerField(default=0)
    pendapatanRabu = models.IntegerField(default=0)
    pendapatanKamis = models.IntegerField(default=0)
    pendapatanJumat = models.IntegerField(default=0)
    pendapatanSabtu = models.IntegerField(default=0)
    pendapatanMinggu = models.IntegerField(default=0)

class KeuntunganMingguan(models.Model):
    keuntunganSenin = models.IntegerField(default=0)
    keuntunganSelasa = models.IntegerField(default=0)
    keuntunganRabu = models.IntegerField(default=0)
    keuntunganKamis = models.IntegerField(default=0)
    keuntunganJumat = models.IntegerField(default=0)
    keuntunganSabtu = models.IntegerField(default=0)
    keuntunganMinggu = models.IntegerField(default=0)

class PendapatanTahunan(models.Model):
    Jan = models.IntegerField(default=0)
    Feb = models.IntegerField(default=0)
    Mar = models.IntegerField(default=0)
    Apr = models.IntegerField(default=0)
    Mei = models.IntegerField(default=0)
    Jun = models.IntegerField(default=0)
    Jul = models.IntegerField(default=0)
    Aug = models.IntegerField(default=0)
    Sep = models.IntegerField(default=0)
    Okt = models.IntegerField(default=0)
    Nov = models.IntegerField(default=0)
    Dec = models.IntegerField(default=0)

class KeuntunganTahunan(models.Model):
    Jan = models.IntegerField(default=0)
    Feb = models.IntegerField(default=0)
    Mar = models.IntegerField(default=0)
    Apr = models.IntegerField(default=0)
    Mei = models.IntegerField(default=0)
    Jun = models.IntegerField(default=0)
    Jul = models.IntegerField(default=0)
    Aug = models.IntegerField(default=0)
    Sep = models.IntegerField(default=0)
    Okt = models.IntegerField(default=0)
    Nov = models.IntegerField(default=0)
    Dec = models.IntegerField(default=0)