from django.shortcuts import render
from .forms import RestockForm
from .models import Toko, Item
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from app_login.views import group_required

# Create your views here.
response = {}

@login_required
@group_required('Admin')
def index(request):
    form = RestockForm()
    html = 'index_stock_desktop.html'
    response['items'] = Item.objects.all()
    response['tokos'] = Toko.objects.all()
    response['form'] = form
    return render(request, html, response)
    
@group_required('Admin')
@login_required
def restock(request, item_id):
    print('masuk')
    if request.method == "POST":
        form = RestockForm(request.POST)
        if form.is_valid():
            print('valid')
            jumlah = form.cleaned_data['jumlah']
            harga = form.cleaned_data['harga']
            tglBeli = form.cleaned_data['tglBeli']
            item = Item.objects.get(id=item_id)
            item.jumlah_tersedia += jumlah
            item.harga_jual = harga
            item.tanggal_beli = tglBeli
            item.save()
        else :
            print('tidak valid')
    else:
        form = RestockForm()

    return HttpResponseRedirect('/stock')