from celery import task, shared_task
from .models import Toko, Item, DailyItem, DiscrepancyItem

store_ids = ['aPq', "ZoI"]

@shared_task
def reset_store_status():
    for i in store_ids:
        toko = Toko.objects.get(unique_id=i)
        toko.status = False
        toko.save()

@shared_task
def reset_store_discrepancy():
    for i in store_ids:
        toko = Toko.objects.get(unique_id=i)
        toko.discrepancy = False
        toko.save()


# TESTING 
@shared_task
def reset_discrepancy_items():
    DiscrepancyItem.objects.all().delete()
    DailyItem.objects.all().delete()


    