from django.apps import AppConfig


class AppDesktopStockConfig(AppConfig):
    name = 'app_desktop_stock'
