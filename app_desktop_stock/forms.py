from django import forms
from app_desktop_stock.models import Item, Toko

class RestockForm(forms.Form):
    jumlah = forms.IntegerField()
    harga = forms.IntegerField()
    tglBeli = forms.DateField(widget=forms.TextInput(attrs=
                                {
                                    'id':'datepicker'
                                }))


    