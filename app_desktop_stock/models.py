from django.db import models
import datetime

class Toko(models.Model):
    nama = models.CharField(max_length=50)
    unique_id = models.CharField(max_length=10, default='null')
    status = models.BooleanField(default=False)
    discrepancy = models.BooleanField(default=False)

    def __str__(self):
        return self.nama

class Item(models.Model):
    nama = models.CharField(max_length=50)
    toko_dijual = models.ForeignKey(Toko, on_delete=models.CASCADE, related_name='items')
    harga_beli = models.IntegerField(default=0)
    harga_jual = models.IntegerField(default=0)
    jumlah_tersedia = models.IntegerField(default=0)
    tanggal_beli = models.DateField()
    tanggal_jual = models.DateField()
    total_penjualan = models.IntegerField(default=0)

    def __str__(self):
        return self.nama

class DiscrepancyItem(models.Model):
    nama = models.CharField(max_length=50)
    toko_dijual = models.ForeignKey(Toko, on_delete=models.CASCADE, related_name='items_discrepancy')
    jumlah_seharusnya = models.IntegerField(default=0)
    jumlah_asli = models.IntegerField(default=0)

    def __str__(self):
        return self.nama

class DailyItem(models.Model):
    item=models.ForeignKey(Item, on_delete=models.CASCADE)
    jumlah = models.IntegerField(default=0)
