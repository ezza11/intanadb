from django.contrib import admin
from .models import Toko, Item, DiscrepancyItem, DailyItem
# Register your models here.

admin.site.register(Toko)
admin.site.register(Item)
admin.site.register(DiscrepancyItem)
admin.site.register(DailyItem)