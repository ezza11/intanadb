from django.shortcuts import render
from app_desktop_stock.models import Toko, Item, DiscrepancyItem
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from app_login.views import group_required


# Create your views here.
response = {}

@login_required
@group_required('Admin', 'Staff')
def index(request):
    tokos = Toko.objects.all()
    html = 'index_stock_mob.html'
    response['tokos'] = tokos
    return render(request, html, response)

@login_required
@group_required('Admin', 'Staff')
def check(request):
    items = Item.objects.all()
    tokos = Toko.objects.all()
    for i in items:
        groupName = "item-" + str(i.id)
        errorName = "error-" + "item-" + str(i.id)
        value = int(request.POST.get(groupName))
        # Jika jumlah barangnya ga sesuai sama stok
        if value == 0 :
            jumlah_barang = request.POST[errorName]
            if jumlah_barang == '':
                jumlah_barang = 0
                item = DiscrepancyItem(nama=i.nama, toko_dijual=i.toko_dijual, jumlah_seharusnya=i.jumlah_tersedia, jumlah_asli=jumlah_barang)
                item.toko_dijual.discrepancy = True
                item.toko_dijual.save()
                item.save()
            else:
                item = DiscrepancyItem(nama=i.nama, toko_dijual=i.toko_dijual, jumlah_seharusnya=i.jumlah_tersedia, jumlah_asli=jumlah_barang)
                item.toko_dijual.discrepancy = True
                item.toko_dijual.save()
                item.save()
    for i in tokos:
        i.status = True
        i.save()
    return HttpResponseRedirect('/mobile/stock')

    
    
            