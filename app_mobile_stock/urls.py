from django.urls import path

from . import views

urlpatterns = [
    # URL -> localhost:8000/main
    path("", views.index, name='index'),
    path("check", views.check, name='check')
]
