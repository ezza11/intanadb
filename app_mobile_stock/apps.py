from django.apps import AppConfig


class AppMobileStockConfig(AppConfig):
    name = 'app_mobile_stock'
